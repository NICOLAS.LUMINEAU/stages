# Projet d'Ouverture à la Recherche (ex-POM) 2024

Sujets réservés aux étudiants du Master 1 du Département Informatique de l'UCBL 1: [Page UE MIF11](http://sylvain.brandel.pages.univ-lyon1.fr/ouverture/)

   - Sujet POM - shareFAIR 1 / **Disponible** : [Interrogation et visualisation de workflows scientifiques dédiés à l'analyse de données médicales](./2024_POM_Dev/POM_Sujet_dev.md) 
   - Sujet POM - shareFAIR 2 / **Disponible** : [Ordonnancement de résultats pour des requêtes SPARQL partielles](./2024_POM_Bib/POM_Sujet_bib.md) 
