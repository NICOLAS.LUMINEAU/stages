## Sujet d'Ouverture à la Recherche - ShareFAIR 1

Sujet réservé aux étudiants du Master 1 du Département Informatique de l'UCBL 1: [Page UE MIF11](http://sylvain.brandel.pages.univ-lyon1.fr/ouverture/)

# Interrogation et visualisation de workflows scientifiques dédiés à l'analyse de données médicales

## 1. Contexte

D'un point de vue de la santé publique, l'analyse de données médicales est un enjeu crucial dans l'évolution des traitements. Un des verrous principal est l'accès aux données médicales du fait de leur confidentialité. Faute de pouvoir partager les données, la question se pose sur le partage des outils d'analyses.\
Le cadre de ce POM est le projet de recherche *shareFAIR* du PEPR SantéNum (<https://projet.liris.cnrs.fr/sharefair/>). Le laboratoire du LIRIS est impliqué dans le projet afin de définir un langage de haut-niveau permettant à des bio-informaticiens d'identifier des *workflows* (de type *snakeMake* ou *nextFlow*) pertinents à leurs besoins parmi différents corpus disponibles. L'objectif est de pouvoir à terme proposer une approche [FAIR](https://www.ccsd.cnrs.fr/principes-fair/) (Findable Accessible Interoperable Reusable) pour les *workflows* considérés.

![Figure 1 : recherche de workflows](./principe.png)

Comme illustré sur la Figure 1, l'utilisateur final interroge le moteur de requêtes via l'interface web. La requête écrite en un langage à la SQL est analysée et réécrite en SPARQL à l'aide de règles de réécritures définies dans un dictionnaire. Les sources sont alors interrogées et les résultats sont triés selon leur pertinence avant d'être retourné à l'utilisateur.

Jusqu'à présent des prototypes de stages de recherche ont été développés indépendamment. Le premier est un outil web développé en JS et proposant une interface de visualisation de *workflows* avec leur métadonnées (données descriptives du *workflows*). Le second est une application JAVA intégrant un parseur JAVACC pour la réécriture de requête et la production requêtes SPARQL.

## 2. Objectif

Ce projet POM a pour objectif d'analyser l'existant et de proposer une refonte des outils en **[React](https://fr.react.dev/)** pour développer une application web intégrant les aspects interrogation et visualisation des *workflows*.

## 3. Délivrables

Durant le temps consacré au projet POM, l'étudiant.e devra fournir :

* Le cahier des charges de l'application
* La synthèse d'un article
* Le code source de l'application développée
* La documentation de l'application
* Le rapport de clôture du projet POM

## 4. Informations pratiques

Nombre d'étudiants souhaité: 1 stagiaire

Parcours en Master 2 ciblé : TIW

Encadrants :

* Emmanuel COQUERY (emmanuel.coquery@univ-lyon1.fr)
* Nicolas LUMINEAU (nicolas.lumineau@univ-lyon1.fr)