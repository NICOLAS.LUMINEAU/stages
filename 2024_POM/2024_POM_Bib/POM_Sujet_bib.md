## Sujet POM - ShareFAIR 2

# Ordonnancement de résultats pour des requêtes SPARQL partielles

## 1. Contexte

D'un point de vue de la santé publique, l'analyse de données médicales est un enjeu crucial dans l'évolution des traitements. Un des verrous principal est l'accès aux données médicales du fait de leur confidentialité. Faute de pouvoir partager les données, la question se pose sur le partage des outils d'analyses. Le cadre de ce POM est le projet de recherche *shareFAIR* du PEPR SantéNum (<https://projet.liris.cnrs.fr/sharefair/>). Le laboratoire du LIRIS est impliqué dans le projet afin de définir un langage de haut-niveau permettant à des bio-informaticiens d'identifier des *workflows* (de type *snakeMake* ou *nextFlow*) pertinents à leurs besoins parmi différents corpus disponibles. L'objectif est de pouvoir à terme proposer une approche [FAIR](https://www.ccsd.cnrs.fr/principes-fair/) (Findable Accessible Interoperable Reusable) pour les *workflows* considérés.

![Figure 1 : recherche de workflows](./principe.png)

Comme illustré sur la Figure 1, l'utilisateur final interroge le moteur de requêtes via l'interface web. La requête écrite en un langage à la SQL est analysée et réécrite en SPARQL à l'aide de règles de réécritures définies dans un dictionnaire. Les sources sont alors interrogées et les résultats sont triés selon leur pertinence avant d'être retourné à l'utilisateur.

Ce projet se focalise sur le moteur de requêtes qui produit les requêtes SPARQL qui interrogerons les sources et qui trie les résultats selon des critères de pertinences. Dans notre contexte, le processus de réécriture des requêtes introduit des contraintes optionnelles de manière à retourner des résultats mêmes si l'ensemble des contraintes n'est pas satisfait. Ce principe a donc des conséquences sur la stratégie d'ordonnancement des résultats, qu'il est nécessaire d'étudier.

## 2. Objectif

Ce projet POM a pour objectif de faire une étude bibliographique sur le traitement de requêtes partielles en SPARQL et l'ordonnacement des résultats. L'étude pourra s'ammorcer avec les articles suivants :

* Évaluation de requêtes :
  * "Processing SPARQL Queries Over Distributed RDF Graphs" de Peng Peng, Lei Zou, M. Tamer Özsu, Lei Chen, Dongyan Zhao (VLDB'19) [PDF](https://cs.uwaterloo.ca/\~tozsu/publications/rdf/DistgStoreVLDBJ.pdf)
  * "Accelerating Partial Evaluation in Distributed SPARQL Query Evaluation" de Peng Peng, Lei Zou, Runyu Guan (ICDE'19) [PDF](https://arxiv.org/pdf/1902.03700)
  * "On the Semantics of SPARQL Queries with Optional Matching under Entailment Regimes" de Egor V. Kostylev and Bernardo Cuenca Grau (ISWC'14) [PDF](https://www.cs.ox.ac.uk/files/6665/SPARQL-ISWC.pdf)
* Ordonnancement de résultats :
  * "Query-Independent Learning to Rank for RDF Entity Search" de Lorand Dali, Blaž Fortuna, Thanh Tran, Dunja Mladenić (ESWC'12) [PDF](https://ailab.ijs.si/wp-content/uploads/2020/05/Query-independent-learning-to-rank-for-RDF-entity-search.pdf)
  * "PageRank and Generic Entity Summarization for RDF Knowledge Bases" de Dennis Diefenbach, Andreas Thalhammer (ESWC'18) [PDF](https://hal.science/hal-01905724/file/PageRank_and_Generic_Entity_Summarization_for_RDF_Knowledge_Bases.pdf)

## 3. Délivrables

Durant le temps consacré au projet POM, l'étudiant.e devra fournir :

* La synthèse de chaque article lu
* Une synthèse globale des différents articles lus
* Le rapport de clôture du projet POM

## 4. Informations pratiques

Nombre d'étudiants souhaité: 1 stagiaire

Parcours en Master 2 ciblé : non ciblé

Encadrants :

* Emmanuel COQUERY (emmanuel.coquery@univ-lyon1.fr)
* Nicolas LUMINEAU (nicolas.lumineau@univ-lyon1.fr)